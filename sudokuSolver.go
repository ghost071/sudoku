package main

import (
	"fmt"
	//"reflect"
	"time"
	"io"
	"os"
	"strconv"
	"strings"
	"bufio"
)

func readBoard(file string) ([][]int, error) {

	board := make([][]int, 9)
	f, err := os.Open(file)

	if err != nil {
		return board, err
	}

	r := bufio.NewReader(f)

	for k := 0; k < 9; k++ {
		line, err := r.ReadString('\n')
		line = strings.Trim(line, "\n")
		arr := strings.Split(line, "")

		for _, s := range arr {
			num, err := strconv.Atoi(s)
			if err != nil {
				fmt.Println(err)
				return board, err
			}
			board[k] = append(board[k], num)
		}

		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
		}
	}
	return board, nil
}

type Position struct {
	X int
	Y int
	Check map[int]bool
	Next *Position
	Prev *Position
}

const (
	RED = "\033[31m"
	END = "\033[0m"
)

var n int

func isValidNum(board *[][]int, num int, ps *Position) bool {

	if ps.Check[num] {
		return false
	}

	for i := 0; i < len((*board)); i++ {
		if (*board)[ps.X][i] == num {
			return false
		}
	}

	for i := 0; i < len((*board)); i++ {
		if (*board)[i][ps.Y] == num {
			return false
		}
	}

	x, y := ps.X - ps.X % 3, ps.Y - ps.Y % 3
	for i := x; i < x + 3; i++ {
		for j := y; j < y + 3; j++ {
			if (*board)[i][j] == num {
				return false
			}
		}
	}
	return true
}

func solveField(board *[][]int, ps *Position) {
	n++
	if ps == nil {
		return
	}

	for i := 1; i < 10; i++ {

		if isValidNum(board, i, ps) {
			(*board)[ps.X][ps.Y] = i
			ps.Check[i] = true
			solveField(board, ps.Next)
			return
		}

	}

	if ps.Prev == nil {
		fmt.Println("Invalid sudoku. No solutions")
		return
	}

	check := make(map[int]bool)
	ps.Check = check
	ps = ps.Prev
	(*board)[ps.X][ps.Y] = 0
	solveField(board, ps)
}

func makePos(x, y int, n, p *Position) *Position {
	ch := make(map[int]bool)
	return &Position{x, y, ch, n, p}
}

func solve(board *[][]int) {
	head := makePos(0, 0, nil, nil)
	tmp := head
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if (*board)[i][j] == 0 {
				tmp.Next = makePos(i, j, nil, tmp)
				tmp = tmp.Next
			}
		}
	}
	head = head.Next
	head.Prev = nil
	solveField(board, head)
}

func prettyPrint(b [][]int) {

	border := RED + "\n" + strings.Repeat("=", 41) + END
	fmt.Println(border)

	part := strings.Repeat("-", 11)
	part2 := strings.Repeat(RED + "||" + END + part, 3)
	subLine := "\n" + part2 + RED + "||" + END

	printN := func(x int) {
		if x == 0 {
			fmt.Print(" ")
		} else {
			fmt.Print(x)
		}
	}

	for j, arr := range b {
		fmt.Print(RED, "|| ", END)
		for i, el := range arr {
			if i == 2 || i == 5 || i == 8 {
				printN(el)
				fmt.Print(" " + RED + "|| " + END)
				continue
			}
			printN(el)
			fmt.Print(" | ")
		}
		if j == 2 || j == 5 || j == 8 {
			fmt.Println(border)
			continue
		}

		fmt.Println(subLine)
	}
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Wrong number of args")
		return
	}

	fname := os.Args[1]
	board, err := readBoard(fname)

	if err != nil {
		fmt.Println(err)
	}

	prettyPrint(board)
	start := time.Now()
	solve(&board)
	dur := time.Since(start)
	prettyPrint(board)
	fmt.Println(dur, "iterations", n)
}


